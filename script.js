const submitBtn = document.querySelector('#submit');
const intput = document.querySelector('#input');
const btnsArea = document.querySelector('#btnsArea');
let targetBtn;

const handleBtnClick = event => {
    const btn = event.target;
    const {id} = btn;
    if (Number(id) === targetBtn) {
        btn.style.backgroundColor = 'green';        
    } else {
        btn.style.backgroundColor = 'red'; 
    }
};

const generateBtns = nr => {
    btnsArea.innerHTML = '';
    for (let i = 0; i < nr; i++) {
        const btn = document.createElement('button');
        const text = document.createTextNode(`btn-${i}`);
        btn.appendChild(text);
        btn.id = i;
        btn.addEventListener('click', handleBtnClick);
        btnsArea.appendChild(btn);
        btnsArea.appendChild(document.createElement('br'));
    }
};

const isOk = nr => {
    if (nr <= 0 || !Number.isInteger(nr)) {
        return false;
    }
    return true;
};

submitBtn.addEventListener('click', event => {
    const nr = Number(input.value);
    if (!isOk(nr)) {
        alert('Input invalid');
        return;
    }
    targetBtn = Math.floor(Math.random() * nr);
    generateBtns(nr);
    console.log(targetBtn);
});